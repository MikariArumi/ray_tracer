#[derive(Copy, Clone)]
#[repr(simd)]
pub struct Dim3 {
    x: f64,
    y: f64,
    z: f64,
}

pub type Point3 = Dim3;
pub type Vector3 = Dim3;

impl Dim3 {
    pub fn new(x: f64, y: f64, z: f64) -> Self {
        Dim3 { x, y, z }
    }
    #[inline]
    pub fn x(&self) -> f64 {
        self.x
    }

    #[inline]
    pub fn y(&self) -> f64 {
        self.y
    }

    #[inline]
    pub fn z(&self) -> f64 {
        self.z
    }

    pub fn x_add(&mut self, a: f64) {
        self.x += a
    }
    pub fn y_add(&mut self, a: f64) {
        self.y += a
    }
    pub fn z_add(&mut self, a: f64) {
        self.z += a
    }

    pub fn to_color_str(&self) -> String {
        format!(
            "{} {} {}",
            (self.x() * 255.999).clamp(0.0, 255.0) as i32,
            (self.y() * 255.999).clamp(0.0, 255.0) as i32,
            (self.z() * 255.999).clamp(0.0, 255.0) as i32
        )
    }

    #[allow(dead_code)]
    pub fn print(&self) {
        println!("{} {} {}", self.x(), self.y(), self.z());
    }

    #[inline]
    pub fn norm_sqrt(&self) -> f64 {
        self.x() * self.x() + self.y() * self.y() + self.z() * self.z()
    }

    #[inline]
    pub fn norm(&self) -> f64 {
        (self.norm_sqrt()).sqrt()
    }

    pub fn unit_vector(&self) -> Self
    where
        Self: Sized,
    {
        *self / self.norm()
    }

    pub fn clamp(&self) -> Self
    where
        Self: Sized,
    {
        let x = self.x().clamp(0.0, 1.0);
        let y = self.y().clamp(0.0, 1.0);
        let z = self.z().clamp(0.0, 1.0);

        Self::new(x, y, z)
    }

    #[inline]
    pub fn cross(&self, rhs: Self) -> Self
    where
        Self: Sized,
    {
        Self::new(
            self.y() * rhs.z() - self.z() * rhs.y(),
            self.z() * rhs.x() - self.x() * rhs.z(),
            self.x() * rhs.y() - self.y() * rhs.x(),
        )
    }

    #[inline]
    pub fn dot(&self, other: Self) -> f64
    where
        Self: Sized,
    {
        self.x() * other.x() + self.y() * other.y() + self.z() * other.z()
    }
}

impl std::cmp::PartialEq<Dim3> for Dim3 {
    fn eq(&self, other: &Self) -> bool {
        self.x() == other.x() && self.y() == other.y() && self.z() == other.z()
    }
}
impl std::cmp::Eq for Dim3 {}

impl std::ops::Mul<Dim3> for Dim3 {
    type Output = Self;

    fn mul(self, other: Self) -> Self {
        Self::new(
            self.x() * other.x(),
            self.y() * other.y(),
            self.z() * other.z(),
        )
    }
}

impl std::ops::Mul<Dim3> for f64 {
    type Output = Dim3;

    fn mul(self, dim: Dim3) -> Self::Output {
        Self::Output::new(dim.x() * self, dim.y() * self, dim.z() * self)
    }
}

impl std::ops::Mul<f64> for Dim3 {
    type Output = Self;

    fn mul(self, scalar: f64) -> Self {
        Self::new(self.x() * scalar, self.y() * scalar, self.z() * scalar)
    }
}

impl std::ops::Div<Dim3> for Dim3 {
    type Output = Self;

    fn div(self, other: Self) -> Self {
        Self::new(
            self.x() / other.x(),
            self.y() / other.y(),
            self.z() / other.z(),
        )
    }
}

impl std::ops::Div<f64> for Dim3 {
    type Output = Self;

    fn div(self, scalar: f64) -> Self {
        Self::new(self.x() / scalar, self.y() / scalar, self.z() / scalar)
    }
}

impl std::ops::Sub<Dim3> for Dim3 {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        Self::new(
            self.x() - other.x(),
            self.y() - other.y(),
            self.z() - other.z(),
        )
    }
}

impl std::ops::Sub<Dim3> for f64 {
    type Output = Dim3;

    fn sub(self, dim: Dim3) -> Self::Output {
        Self::Output::new(self - dim.x(), self - dim.y(), self - dim.z())
    }
}
impl std::ops::Sub<f64> for Dim3 {
    type Output = Self;

    fn sub(self, scalar: f64) -> Self {
        Self::new(self.x() - scalar, self.y() - scalar, self.z() - scalar)
    }
}

impl std::ops::Add<Dim3> for Dim3 {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self::new(
            self.x() + other.x(),
            self.y() + other.y(),
            self.z() + other.z(),
        )
    }
}
impl std::ops::Add<Dim3> for f64 {
    type Output = Dim3;

    fn add(self, dim: Dim3) -> Self::Output {
        Self::Output::new(self + dim.x(), self + dim.y(), self + dim.z())
    }
}
impl std::ops::Add<f64> for Dim3 {
    type Output = Self;

    fn add(self, scalar: f64) -> Self {
        Self::new(self.x() + scalar, self.y() + scalar, self.z() + scalar)
    }
}

impl std::ops::AddAssign<Dim3> for Dim3 {
    fn add_assign(&mut self, other: Self) {
        self.x_add(other.x());
        self.y_add(other.y());
        self.z_add(other.z());
    }
}

impl std::ops::AddAssign<f64> for Dim3 {
    fn add_assign(&mut self, scalar: f64) {
        self.x_add(scalar);
        self.y_add(scalar);
        self.z_add(scalar);
    }
}
