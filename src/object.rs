use crate::dim3::*;
use crate::ray::*;
use crate::texture_material::*;
use crate::ERROR_D;

pub trait Object: ObjectClone + Send + Sync {
    fn has_hit(&self, ray: Ray) -> f64;
    fn normal(&self, point: Point3, ray: Vector3) -> Vector3;
    fn get_texture(&self) -> &Box<dyn TextureMaterial>;
    fn get_s(&self, ray: Ray, hitpoint: Point3) -> Vector3;
    fn get_t(&self, ray: Ray, hitpoint: Point3, n1: f64) -> Vector3;
    fn center(&self) -> Point3;
    fn refract_indice(&self) -> f64;
    fn kt(&self) -> f64;
}

pub trait ObjectClone {
    fn clone_box(&self) -> Box<dyn Object>;
}

impl<T> ObjectClone for T
where
    T: 'static + Object + Clone,
{
    fn clone_box(&self) -> Box<dyn Object> {
        Box::new(self.clone())
    }
}

impl Clone for Box<dyn Object> {
    fn clone(&self) -> Box<dyn Object> {
        self.clone_box()
    }
}

#[derive(Clone)]
pub struct Sphere {
    tex_mat: Box<dyn TextureMaterial>,
    center: Point3,
    radius: f64,
}

#[derive(Clone)]
pub struct Triangle {
    tex_mat: Box<dyn TextureMaterial>,
    p1: Point3,
    p2: Point3,
    p3: Point3,
    n: Vector3,
    edge1: Vector3,
    edge2: Vector3,
}

impl Triangle {
    pub fn new(tex_mat: Box<dyn TextureMaterial>, p1: Point3, p2: Point3, p3: Point3) -> Self {
        let u_x = p2 - p1;
        let u_y = p3 - p1;
        let n = u_x.cross(u_y).unit_vector();

        Triangle {
            tex_mat,
            p1,
            p2,
            p3,
            n,
            edge1: p2 - p1,
            edge2: p3 - p1,
        }
    }
}

impl Object for Triangle {
    #[allow(clippy::many_single_char_names)]
    fn has_hit(&self, r: Ray) -> f64 {
        let h = r.direction().cross(self.edge2);
        let a = h.dot(self.edge1);
        if a.abs() < ERROR_D {
            return -1.0;
        };
        let f = 1.0 / a;
        let s = r.origin() - self.p1;
        let u = f * s.dot(h);
        if !(0.0..=1.0).contains(&u) {
            return -1.0;
        }
        let q = s.cross(self.edge1);
        let v = f * q.dot(r.direction());
        if v < 0.0 || u + v > 1.0 {
            return -1.0;
        }
        let t = f * q.dot(self.edge2);
        if t > ERROR_D {
            return t;
        }
        -1.0
    }

    fn normal(&self, _: Point3, _: Vector3) -> Vector3 {
        self.n
    }

    fn get_texture(&self) -> &Box<dyn TextureMaterial> {
        &self.tex_mat
    }

    fn get_s(&self, ray: Ray, hitpoint: Point3) -> Vector3 {
        let mut n = self.normal(hitpoint, ray.direction());
        if ray.direction().dot(n) > 0.0 {
            n = n * -1.0;
        }
        (ray.direction() + n * -2.0 * n.dot(ray.direction())).unit_vector()
    }

    fn get_t(&self, ray: Ray, hitpoint: Point3, n1: f64) -> Vector3 {
        let mut n = self.normal(hitpoint, ray.direction());

        let mut cos_i = ray.direction().dot(n);
        if cos_i < ERROR_D {
            cos_i *= -1.0;
        } else {
            n = -1.0 * n;
        }
        let sin_t = 1.0 - (n1 * n1) * (1.0 - (cos_i * cos_i));
        if sin_t < ERROR_D {
            return ray.direction();
        }
        (ray.direction() + cos_i * n) * n1 - n * sin_t.sqrt()
    }

    fn center(&self) -> Point3 {
        Point3::new(
            (self.p1.x() + self.p2.x() + self.p3.x()) / 3.0,
            (self.p1.y() + self.p2.y() + self.p3.y()) / 3.0,
            (self.p1.z() + self.p2.z() + self.p3.z()) / 3.0,
        )
    }

    fn refract_indice(&self) -> f64 {
        self.tex_mat.refract_indice()
    }
    fn kt(&self) -> f64 {
        self.tex_mat.kt()
    }
}

#[allow(dead_code)]
impl Sphere {
    pub fn new(tex_mat: Box<dyn TextureMaterial>, center: Point3, radius: f64) -> Self {
        Sphere {
            tex_mat,
            center,
            radius,
        }
    }
}
#[allow(dead_code)]
impl Object for Sphere {
    fn has_hit(&self, ray: Ray) -> f64 // renturn t
    {
        let orig_cent = ray.origin() - self.center;
        let a = ray.direction().norm_sqrt();
        let b = orig_cent.dot(ray.direction()) * 2.0;
        let c = orig_cent.norm_sqrt() - self.radius * self.radius;
        let delta = b * b - 4.0 * a * c;

        if delta < 0.0 {
            return -1.0;
        }
        let sqrtd = delta.sqrt();
        let root1 = (-b + sqrtd) / (2.0 * a);
        let root2 = (-b - sqrtd) / (2.0 * a);
        let mut t = -1.0;

        if root1 > 0.0 && root2 > 0.0 {
            t = root1.min(root2);
        } else if root1 > 0.0 {
            t = root1;
        } else if root2 > 0.0 {
            t = root2;
        }
        if t < ERROR_D {
            return -1.0;
        }
        t
    }

    fn normal(&self, point: Point3, _: Vector3) -> Vector3 {
        (point - self.center) / self.radius
    }

    fn get_texture(&self) -> &Box<dyn TextureMaterial> {
        &self.tex_mat
    }

    fn get_s(&self, ray: Ray, hitpoint: Point3) -> Vector3 {
        let outward_n = (hitpoint - self.center) / self.radius;
        (ray.direction() + outward_n * -2.0 * outward_n.dot(ray.direction())).unit_vector()
    }

    fn get_t(&self, ray: Ray, hitpoint: Point3, n1: f64) -> Vector3 {
        let n = self.normal(hitpoint, ray.direction()).unit_vector();
        let incendence = ray.direction().unit_vector();
        let r = n1 / self.tex_mat.kt();
        let c = incendence.dot(n * -1.0);

        let res: Vector3 = r * incendence + (r * c - ((1.0 - r * r) * (1.0 - c * c)).sqrt()) * n;
        res
        //    ray.direction()//.unit_vector()
    }

    fn center(&self) -> Point3 {
        self.center
    }

    fn refract_indice(&self) -> f64 {
        self.tex_mat.refract_indice()
    }
    fn kt(&self) -> f64 {
        self.tex_mat.kt()
    }
}
