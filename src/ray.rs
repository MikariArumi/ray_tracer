use crate::dim3::*;
use crate::light::*;
use crate::object::*;
use crate::*;
use rand::{thread_rng, Rng};

#[derive(Copy, Clone)]
pub struct Ray {
    origin: Point3,
    direction: Vector3,
}

impl Ray {
    pub fn new(origin_: Point3, direction_: Vector3) -> Self {
        Ray {
            origin: origin_,
            direction: direction_,
        }
    }

    pub fn at(&self, t: f64) -> Point3 {
        self.origin + self.direction * t
    }

    pub fn origin(&self) -> Point3 {
        self.origin
    }

    pub fn direction(&self) -> Vector3 {
        self.direction
    }

    pub fn closest(
        &self,
        objects: &Vec<Box<dyn Object>>,
        max: f64,
    ) -> (Option<Box<dyn Object>>, f64) {
        let mut closest_dist = max;
        let mut closest_obj: Option<Box<dyn Object>> = None;
        if max != f64::MAX {
            for object in objects {
                let hit = object.has_hit(*self);
                if hit > ERROR_D && hit < closest_dist {
                    return (Some(object.clone()), 0.0);
                }
            }
            (None, 0.0)
        } else {
            for object in objects {
                let hit = object.has_hit(*self);
                if hit > ERROR_D && hit < closest_dist {
                    closest_dist = hit;
                    closest_obj = Some(object.clone());
                }
            }
            if closest_dist == max {
                (None, 0.0)
            } else {
                (closest_obj, closest_dist)
            }
        }
    }

    pub fn cast(
        &self,
        obj: &Box<dyn Object>,
        objects: &Vec<Box<dyn Object>>,
        lights: &Vec<Box<dyn Light>>,
        hitpoint: Point3,
        depth: u8,
        refract_indice: f64,
    ) -> Vector3
    {
        let s = obj.get_s(*self, hitpoint);
        let t = obj.get_t(*self, hitpoint, refract_indice);
        let texture = obj.get_texture();
        let mut color = Vector3::new(0.0, 0.0, 0.0);

        if depth == 0 {
            return color;
        }

        //calcul id + is
        for light in lights {
            let dist = (light.pos() - hitpoint).norm();
            let dir_light = (light.pos() - hitpoint).unit_vector();
            let point = hitpoint + dir_light * 0.1;
            let light_ray = Ray::new(point, dir_light);
            let mut normal = obj.normal(hitpoint, self.direction()).unit_vector();
            if self.direction().dot(normal) > 0.0 {
                normal = normal * -1.0;
            }
            if dir_light.dot(normal) > 0.0 {
                let (object, dist_obj) = light_ray.closest(objects, dist);
                if let Some(object) = object {
                    if object.kt() != 0.0 {
                        color += texture.color(hitpoint)
                            * object.get_texture().color(light_ray.at(dist_obj));
                    }
                } else {
                    let dot = dir_light.dot(normal);
                    let id = texture.color(hitpoint) * texture.kd() * dot * light.intensity();
                    let is = s.dot(dir_light).powf(texture.ns()) * texture.ks() * light.intensity();
                    color += is + id;
                }
            }
        }
        //calcul it recurssion
        if texture.ks() != 0.0 {
            let point = hitpoint + s * 0.1;
            let ray = Ray::new(point, s);
            let (obj, hit) = ray.closest(objects, f64::MAX);

            if let Some(obj) = obj {
                let refract = obj.refract_indice();
                color += ray.cast(&obj, objects, lights, ray.at(hit), depth - 1, refract);
            } else {
                color += color_sky(ray) * texture.ks();
            }
        }
        if texture.kt() != 0.0 {
            let point = hitpoint + t * 0.1;
            let ray = Ray::new(point, t);
            let refract = obj.refract_indice();
            let (obj, hit) = ray.closest(objects, f64::MAX);
            if let Some(obj) = obj {
                color += ray.cast(&obj, objects, lights, ray.at(hit), depth - 1, refract);
            } else {
                color += color_sky(ray) * texture.kt();
            }
        }

        color.clamp()
    }

    pub fn random(&self, delta: f64) -> Self {
        let r1: f64 = thread_rng().gen_range(-delta, delta);
        let r2: f64 = thread_rng().gen_range(-delta, delta);
        let r3: f64 = thread_rng().gen_range(-delta, delta);

        let x = self.direction.x() + r1;
        let y = self.direction.y() + r2;
        let z = self.direction.z() + r3;
        let random_dir = Point3::new(x, y, z).unit_vector();
        Self::new(self.origin, random_dir)
    }
}

pub fn color_sky(ray: Ray) -> Vector3 {
    let unit_direction = ray.direction().unit_vector();
    let t = 0.5 * (unit_direction.y() + 1.0);
    ((Vector3::new(1.0, 1.0, 1.0) * (1.0 - t)) + (Vector3::new(0.5, 0.7, 1.0) * t)).clamp()
}
