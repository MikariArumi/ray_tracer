use crate::dim3::*;
use crate::ray;

#[derive(Copy, Clone)]
pub struct Camera {
    center: Point3,
    look_p: Point3,
    up: Vector3,
    zmin: f64,
    alpha: f64,
    beta: f64,
}

impl Camera {
    pub fn new(
        center: Point3,
        look_p: Point3,
        up: Vector3,
        zmin: f64,
        alpha: f64,
        beta: f64,
    ) -> Self {
        let dist = look_p - center;
        let up = dist.cross(up).cross(dist);
        Camera {
            center,
            look_p,
            up,
            zmin,
            alpha,
            beta,
        }
    }

    #[allow(dead_code)]
    pub fn default() -> Self {
        Camera {
            center: Point3::new(0.0, 0.0, 0.0),
            look_p: Point3::new(0.0, 0.0, 1.0),
            up: Vector3::new(0.0, 1.0, 0.0),
            zmin: 1.0,
            alpha: 0.7,
            beta: 0.7,
        }
    }

    pub fn zmin(&self) -> f64 {
        self.zmin
    }
    pub fn alpha(&self) -> f64 {
        self.alpha
    }

    pub fn beta(&self) -> f64 {
        self.beta
    }

    pub fn get_ray(&self, u: f64, v: f64) -> ray::Ray {
        let mut dir = (self.look_p - self.center).unit_vector();
        let up = self.up.unit_vector();
        let right = dir.cross(up).unit_vector();
        dir = dir * self.zmin + up * v + right * u;
        ray::Ray::new(self.center, dir.unit_vector())
    }

    pub fn clone_x(&self, x: f64) -> Self {
        Camera::new(
            self.center + Point3::new(x, 0.0, 0.0),
            self.look_p,
            self.up,
            self.zmin,
            self.alpha,
            self.beta,
        )
    }
}
