use std::fs::File;
use std::io::prelude::*;
use std::io::stdout;
use std::sync::Arc;

use crate::dim3::*;
use crate::scene::*;

use std::sync::mpsc::channel;
use threadpool::ThreadPool;

use crate::noise::*;

#[derive(Clone)]
pub struct Image {
    height: i32,
    width: i32,
    pixels: Vec<Vector3>,
    max_depth: u8,
}

impl Image {
    pub fn new(height: i32, width: i32, max_depth: u8) -> Self {
        Image {
            height,
            width,
            pixels : Image::init_pixels(height, width),
            max_depth,
        }
    }

    fn init_pixels(height : i32, width : i32) -> Vec<Vector3> {
        let mut pixels : Vec<Vector3> = Vec::new();
        for _ in 0..(height * width) {
            pixels.push(Vector3::new(0.0, 0.0, 0.0));
        }
        pixels
    }

    /*
     * \breif average the color of a pixel
     * \param i j : coordonate of pixel to be changed
     *
     * \return color of new pixel
     */
    pub fn average(&self, i: i32, j: i32) -> Vector3 {
        let width_r = self.width - 1;
        let height_r = self.height - 1;
        match i {
            0 => {
                match j {
                    0 => { self.average_top_l_corner() }
                    j if j == height_r => { self.average_bottom_l_corner(j) }
                    j => { self.average_middle_l(j) }
                }
            }
            i if i == width_r => {
                match j{
                    0 => { self.average_top_r_corner(i) }
                    j if j == height_r => { self.average_bottom_r_corner(i, j) }
                    j => { self.average_middle_r(i, j) }
                }
            }
            i => {
                match j {
                    0 => { self.average_middle_top(i) }
                    j if j == height_r => { self.average_middle_bottom(i, j) }
                    j => { self.average_middle(i, j) }
                }
            }
        }
    }


    //i==0 && j ==0
    fn average_top_l_corner(&self) -> Vector3 {
        self.pixels[0] / 3.0
            + self.pixels[1] / 3.0
            + self.pixels[self.width as usize] / 3.0
    }

    //i == 0 && j == height_r
    fn average_bottom_l_corner(&self, j : i32) -> Vector3 {
        self.pixels[(j * self.width) as usize] / 3.0
            + self.pixels[(1 + j * self.width) as usize] / 3.0
            + self.pixels[((j - 1) * self.width) as usize] / 3.0
    }
    
    //i == 0 && j
    fn average_middle_l(&self, j : i32) -> Vector3 {
        self.pixels[(j * self.width) as usize] / 5.0
            + self.pixels[((j - 1) * self.width) as usize] / 5.0
            + self.pixels[(1 + j * self.width) as usize] / 5.0
            + self.pixels[((j + 1) * self.width) as usize] / 5.0
    }
    
    //i == widht_r && j == 0
    fn average_top_r_corner(&self, i : i32) -> Vector3 {
        self.pixels[i as usize] / 3.0
            + self.pixels[(i - 1) as usize] / 3.0
            + self.pixels[(i + self.width) as usize] / 3.0
    }

    //i == widht_r && j == height_r
    fn average_bottom_r_corner(&self, i : i32, j : i32) -> Vector3 {
        self.pixels[(i + j * self.width) as usize] / 3.0
            + self.pixels[(i - 1 + j * self.width) as usize] / 3.0
            + self.pixels[(i + (j - 1) * self.width) as usize] / 3.0
    }

    //i = width_r && j
    fn average_middle_r(&self, i : i32, j : i32) -> Vector3 {
        self.pixels[(i + j * self.width) as usize] / 5.0
            + self.pixels[(i - 1 + j * self.width) as usize] / 5.0
            + self.pixels[(i + (j - 1) * self.width) as usize] / 5.0
            + self.pixels[(i + (j + 1) * self.width) as usize] / 5.0
    }

    //i && j == 0
    fn average_middle_top(&self, i : i32) -> Vector3 {
        self.pixels[i as usize] / 3.0
            + self.pixels[(i + 1) as usize] / 3.0
            + self.pixels[(i - 1) as usize] / 3.0
            + self.pixels[(i + self.width) as usize] / 3.0
    }

    //i && j == height_r
    fn average_middle_bottom(&self, i : i32, j : i32) -> Vector3 {
        self.pixels[(i + j * self.width) as usize] / 3.0
            + self.pixels[(i + 1 + j * self.width) as usize] / 3.0
            + self.pixels[(i - 1 + j * self.width) as usize] / 3.0
            + self.pixels[(i + (j - 1) * self.width) as usize] / 3.0
    }

    // i && j
    fn average_middle(&self, i : i32, j : i32) -> Vector3 {
        self.pixels[(i + j * self.width) as usize] / 5.0
            + self.pixels[(i - 1 + j * self.width) as usize] / 5.0
            + self.pixels[(i + (j - 1) * self.width) as usize] / 5.0
            + self.pixels[(i + 1 + j * self.width) as usize] / 5.0
            + self.pixels[(i + (j + 1) * self.width) as usize] / 5.0
    }


    pub fn save_to_ppm(&self, file_name: &str) -> std::io::Result<()> {
        let mut file = File::create(file_name)?;

        let header = format!("P3\n{} {}\n255\n", self.width, self.height);
        file.write_all(header.as_bytes())?;

        for j in (0..self.height).rev() {
            for i in 0..self.width {
                let color = self.average(i, j);
                file.write_all(color.to_color_str().as_bytes())?;
                file.write_all(b" ")?;
            }
            file.write_all(b"\n")?;
        }
        Ok(())
    }

    /*
     * \brief save image to gray image
     * \param file_name : name of file to be saved
     *
     */
    #[allow(dead_code)]
    pub fn save_to_ppm2(&self, file_name: &str) -> std::io::Result<()> {
        let mut file = File::create(file_name)?;

        let header = format!("P2\n{} {}\n255\n", self.width, self.height);
        file.write_all(header.as_bytes())?;
        for i in 0..self.height {
            for j in 0..self.width {
                let i = i as f64;
                let j = j as f64;
                let color = (perlin(
                    i / self.height as f64 * 10.0,
                    j / self.width as f64 * 10.0,
                    1.0,
                ) * 0.5
                    + 0.5)
                    .clamp(0.0, 1.0)
                    * 255.0;

                file.write_all((color as i32).to_string().as_bytes())?;
                file.write_all(b" ")?;
            }
            file.write_all(b"\n")?;
        }
        Ok(())
    }

    fn display_loading_bar(&self, posi : i32, done : &mut i32, stdout : &mut std::io::Stdout)
    {

        let pos = posi * 100 / (self.width * self.height);
        if pos > *done {
            *done = pos;
            stdout.flush().unwrap();
            let (w_t, _) = term_size::dimensions().unwrap();
            print!("\r{}% done ", pos);
            for _ in 0..(pos - 9 * pos / w_t as i32) {
                print!("#")
            }
        }
    }

    pub fn ray_trace(&mut self, world: Scene) {
        let cam = world.cam();
        let unit_x = 2.0 * cam.zmin() * (cam.alpha() / 2.0).tan();
        let unit_y = (2.0 * cam.zmin() * (cam.beta() / 2.0).tan()) / (16.0 / 9.0);

        //multithread
        let pool = ThreadPool::new(num_cpus::get() - 2);
        let (tx, rx) = channel();
        let max_depth = self.max_depth;
        let width = self.width;
        let height = self.height;
        let world = Arc::new(world);

        for k in 0..num_cpus::get() {
            for j in (k * height as usize / num_cpus::get())
                ..((k + 1) * height as usize / num_cpus::get())
                {
                    let world = Arc::clone(&world);
                    let tx = tx.clone();
                    pool.execute(move || {
                        for i in 0..width {
                            let u = (i as f64) * unit_x / (width as f64) - unit_x / 2.0;
                            let v = (j as f64) * unit_y / (height as f64) - unit_y / 2.0;
                            let ray = world.cam().get_ray(u, v);
                            let color = world.pixel_color(ray, max_depth).clamp();
                            let mut res = tx.send((j, i, color));
                            while res.is_err() {
                                res = tx.send((j, i, color));
                            }
                        }
                    })
                }
        }

        let mut done = 0;
        let mut stdout = stdout();

        for posi in 0..(self.width * self.height) {

            self.display_loading_bar(posi, &mut done, &mut stdout);

            let (j, i, color) = rx.recv().unwrap();
            self.pixels[(j * self.width as usize + i as usize) as usize] = color;
        }
        println!();
    }
}
