use crate::dim3::*;
use crate::object::Object;
use crate::object::Triangle;
use crate::texture_material::*;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::str::FromStr;

pub fn mtl_parser(filename: &str) -> Vec<(String, UniformTexture)> {
    let mut texture_vec = Vec::new();
    let file = File::open(format!("{}.mtl", filename));
    if file.is_err() {
        return texture_vec;
    }
    let file = file.unwrap();
    let reader = BufReader::new(&file);

    let mut name = String::new();
    let mut color = Point3::new(0.0, 0.0, 0.0);
    let mut in_mtl = false;
    for line in reader.lines() {
        let mut line = line.unwrap();
        if line.starts_with('#') {
            continue;
        }

        if in_mtl {
            line.retain(|c| c != 9 as char);
            let mut split = line.split(' ');
            match split.next() {
                Some("Kd") => {
                    let r = split.next().unwrap().parse().unwrap();
                    let g = split.next().unwrap().parse().unwrap();
                    let b = split.next().unwrap().parse().unwrap();
                    color = Point3::new(r, g, b);
                }
                Some("Ka") => {}
                Some("Ks") => {}
                Some("") => {
                    let uni_tex = UniformTexture::new(color, 1.0, 0.0, 10000.0);
                    texture_vec.push((name.clone(), uni_tex));
                    in_mtl = true;
                }
                _ => {}
            }
        }
        let mut split = line.split(' ');
        if split.next() == Some("newmtl") {
            name = split.next().unwrap().to_string();
            in_mtl = true;
        }
    }
    if in_mtl {
        let uni_tex = UniformTexture::new(color, 1.0, 0.4, 10000.0);
        texture_vec.push((name, uni_tex));
    }

    texture_vec
}

pub fn obj_parser(filename: &str) -> Vec<Box<dyn Object>> {
    let mut points = Vec::new();
    let mut obj_vec: Vec<Box<dyn Object>> = Vec::new();
    let file = File::open(format!("{}.obj", filename)).unwrap();
    let mtl_texture: Vec<(String, UniformTexture)> = mtl_parser(filename);
    let reader = BufReader::new(file);
    let offset = Point3::new(0.0, 1.5, 0.0);
    let mut current_color = UniformTexture::new(Point3::new(0.7, 0.7, 0.7), 1.0, 0.0, 10000.0);
    for (_, line) in reader.lines().enumerate() {
        let line = line.unwrap();
        match (line.chars().next(), line.chars().nth(1)) {
            (Some('u'), Some('s')) => {
                current_color = get_color(line, current_color, &mtl_texture)
            }
            (Some('v'), Some(' ')) => {
               add_vertices(line, &mut points, offset); 
            }
            (Some('f'), Some(' ')) => {
                let mut current_point = Vec::new();

                add_face(line, &mut current_point);

                if current_point.len() == 3 {
                    add_triangle(&mut obj_vec, &current_color, &mut points, &current_point);
                }
                if current_point.len() == 4 {
                    add_square(&mut obj_vec, &current_color, &mut points, &current_point)
                }

            }
            (_, _) => {}
        }
    }
    obj_vec
}

fn add_vertices(line : String, points : &mut Vec<Point3>, offset : Point3)
{
    let mut current_point = Vec::new();
    for i in line.split(' ') {
        if i != "v" {
            let pos = f64::from_str(i);
            if let Ok(pos) = pos {
                current_point.push(pos);
            }
        }
    }
    if current_point.len() == 3 {
        points.push(Point3::new(current_point[0], current_point[1], current_point[2]) + offset);
    }
}

fn add_face(line : String, current_point : &mut Vec<f64>)
{

    let mut tmp_texture_vec = Vec::new();
    for i in line.split(' ') {
        if i != "f" {
            let mut split = i.split('/');
            let first = split.next().unwrap();
            let pos = f64::from_str(first);
            if let Ok(pos) = pos {
                current_point.push(pos);
            }
            let second = split.next();
            if let Some(second) = second {
                let color = f64::from_str(second);
                if let Ok(color) = color {
                    tmp_texture_vec.push(color);
                }
            }
        }
    }

}

fn get_color(line : String, current_color : UniformTexture, mtl_texture : &Vec<(String, UniformTexture)> ) -> UniformTexture
{
    let mut tmp = line.split(' ');
    tmp.next();
    let find = tmp.next().unwrap();
    for (name, texture) in mtl_texture {
        if name == find {
            texture.color(Point3::new(0.0, 0.0, 0.0));
            return texture.clone();
        }
    }
    current_color
}

fn add_triangle(obj_vec : &mut Vec<Box<dyn Object>>, current_color : &UniformTexture, points : &Vec<Point3>, current_point : &Vec<f64>)
{
    let uni_tex = current_color.clone();
    obj_vec.push(Box::new(Triangle::new(
                Box::new(uni_tex),
                points[current_point[0] as usize - 1],
                points[current_point[1] as usize - 1],
                points[current_point[2] as usize - 1],
    )));
}

fn add_square(obj_vec : &mut Vec<Box<dyn Object>>, current_color : &UniformTexture, points : &Vec<Point3>, current_point : &Vec<f64>)
{
    let uni_tex = current_color.clone();
    obj_vec.push(Box::new(Triangle::new(
                Box::new(uni_tex),
                points[current_point[0] as usize - 1],
                points[current_point[1] as usize - 1],
                points[current_point[2] as usize - 1],
    )));
    let uni_tex = current_color.clone();
    obj_vec.push(Box::new(Triangle::new(
                Box::new(uni_tex),
                points[current_point[2] as usize - 1],
                points[current_point[3] as usize - 1],
                points[current_point[0] as usize - 1],
    )));
}
