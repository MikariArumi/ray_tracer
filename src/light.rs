use crate::dim3::*;

pub trait Light: LightClone + Send + Sync {
    fn intensity(&self) -> f64;
    fn pos(&self) -> Point3;
}

pub trait LightClone {
    fn clone_box(&self) -> Box<dyn Light>;
}

impl<T> LightClone for T
where
    T: 'static + Light + Clone,
{
    fn clone_box(&self) -> Box<dyn Light> {
        Box::new(self.clone())
    }
}

impl Clone for Box<dyn Light> {
    fn clone(&self) -> Box<dyn Light> {
        self.clone_box()
    }
}

#[derive(Clone)]
pub struct PointLight {
    pos: Point3,
    intensity: f64,
}

impl PointLight {
    pub fn new(pos: Point3, intensity: f64) -> PointLight {
        PointLight { pos, intensity }
    }
}

impl Light for PointLight {
    fn intensity(&self) -> f64 {
        self.intensity
    }
    fn pos(&self) -> Point3 {
        self.pos
    }
}
