#![feature(repr_simd)]

mod camera;
mod dim3;
mod image;
mod input;
mod light;
mod noise;
mod obj;
mod object;
mod ray;
mod scene;
mod texture_material;

use crate::camera::Camera;
use crate::dim3::*;
use crate::input::*;
use crate::light::*;
use crate::scene::Scene;
use crate::scene::*;
use std::env;

static ERROR_D: f64 = 0.0000001;

/// Args:
/// h w [Object]* [Sea] Camera
/// Object: object name without extension
/// Sea: x, y, precision, rougness, coef of beach
fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        return;
    }

    let (mut img, object, light, camera) = input_parser_dispatcher(&args[1..]).unwrap();
    for (cam_nb, i) in camera.iter().enumerate() {
        let world = Scene::new(object.clone(), light.clone(), *i);
        img.ray_trace(world);
        let has_saved = img.save_to_ppm(&format!("image_{}.ppm", cam_nb).to_string());
        if has_saved.is_ok() {
            println!("image_{}.ppm has been successfully saved", cam_nb);
        } else {
            println!("image_{}.ppm could not be saved", cam_nb);
        }
    }
}
