use crate::dim3::*;

pub trait TextureMaterial: TextureClone + Send + Sync {
    fn color(&self, pos: Point3) -> Vector3;
    fn kd(&self) -> f64;
    fn ks(&self) -> f64;
    fn kt(&self) -> f64;
    fn refract_indice(&self) -> f64;
    fn ns(&self) -> f64;
}
pub trait TextureClone {
    fn clone_box(&self) -> Box<dyn TextureMaterial>;
}

impl<T> TextureClone for T
where
    T: 'static + TextureMaterial + Clone,
{
    fn clone_box(&self) -> Box<dyn TextureMaterial> {
        Box::new(self.clone())
    }
}

impl Clone for Box<dyn TextureMaterial> {
    fn clone(&self) -> Box<dyn TextureMaterial> {
        self.clone_box()
    }
}

#[derive(Clone)]
pub struct WaterTexture {
    color: Vector3,
    kd: f64,
    ks: f64,
    kt: f64,
    refract_indice: f64,
    ns: f64,
}
impl WaterTexture {
    pub fn new() -> Self {
        Self {
            color: Vector3::new(0.0, 0.05, 0.53),
            kd: 0.9,
            ks: 0.01,
            kt: 0.08,
            refract_indice: 1.333,
            ns: 10000.0,
        }
    }
}
impl TextureMaterial for WaterTexture {
    fn color(&self, _pos: Point3) -> Vector3 {
        self.color
    }

    fn kd(&self) -> f64 {
        self.kd
    }
    fn kt(&self) -> f64 {
        self.kt
    }
    fn ks(&self) -> f64 {
        self.ks
    }
    fn ns(&self) -> f64 {
        self.ns
    }
    fn refract_indice(&self) -> f64 {
        self.refract_indice
    }
}

#[derive(Clone)]
pub struct UnderWaterTexture {
    color: Vector3,
    kd: f64,
    ks: f64,
    kt: f64,
    refract_indice: f64,
    ns: f64,
}
impl UnderWaterTexture {
    pub fn new() -> Self {
        Self {
            color: Vector3::new(0.0, 0.05, 0.93),
            kd: 0.5,
            ks: 0.0,
            kt: 1.0,
            refract_indice: 1.5,
            ns: 10000.0,
        }
    }
}
impl TextureMaterial for UnderWaterTexture {
    fn color(&self, _pos: Point3) -> Vector3 {
        self.color
    }

    fn kd(&self) -> f64 {
        self.kd
    }
    fn kt(&self) -> f64 {
        self.kt
    }
    fn ks(&self) -> f64 {
        self.ks
    }
    fn ns(&self) -> f64 {
        self.ns
    }
    fn refract_indice(&self) -> f64 {
        self.refract_indice
    }
}

#[derive(Clone)]
pub struct UniformTexture {
    color: Vector3,
    kd: f64,
    ks: f64,
    ns: f64,
}
impl UniformTexture {
    pub fn new(color: Vector3, kd: f64, ks: f64, ns: f64) -> Self {
        Self { color, kd, ks, ns }
    }
}
impl TextureMaterial for UniformTexture {
    fn color(&self, _pos: Point3) -> Vector3 {
        self.color
    }

    fn kd(&self) -> f64 {
        self.kd
    }

    fn kt(&self) -> f64 {
        0.0
    }
    fn ks(&self) -> f64 {
        self.ks
    }
    fn ns(&self) -> f64 {
        self.ns
    }
    fn refract_indice(&self) -> f64 {
        1.0
    }
}
