use std::vec::Vec;

use crate::camera::*;
use crate::dim3::*;
use crate::light::*;
use crate::noise::*;
use crate::obj::obj_parser;
use crate::object::*;
use crate::ray::*;
use crate::texture_material::*;

#[derive(Clone)]
pub struct Scene {
    objects: Vec<Box<dyn Object>>,
    lights: Vec<Box<dyn Light>>,
    cam: Camera,
}

impl Scene {
    pub fn new(objects: Vec<Box<dyn Object>>, lights: Vec<Box<dyn Light>>, cam: Camera) -> Self {
        Scene {
            objects,
            lights,
            cam,
        }
    }

    pub fn pixel_color(&self, ray: Ray, max_depth: u8) -> Vector3 {
        let mut closest_dist = f64::MAX;
        let mut closest_obj: Option<&Box<dyn Object>> = None;

        for obj in &self.objects {
            let hit = obj.has_hit(ray);
            if hit > 0.0 && hit < closest_dist {
                closest_dist = hit;
                closest_obj = Some(obj);
            }
        }
        if let Some(closest_obj) = closest_obj {
            ray.cast(
                &closest_obj,
                &self.objects,
                &self.lights,
                ray.at(closest_dist),
                max_depth,
                1.0,
            )
        } else {
            color_sky(ray)
        }
    }

    pub fn cam(&self) -> &Camera {
        &self.cam
    }
}

pub fn init_beach(
    height: usize,
    width: usize,
    precision: usize,
    frame: f64,
    c: f64,
    coef: f64,
) -> Vec<Box<dyn Object>> {
    let mut objects: Vec<Box<dyn Object>> = Vec::new();
    let yellow: Vector3 = Vector3::new(1.0, 1.0, 0.5);

    let h_ = height as f64;
    let w_ = width as f64;
    let r_ = 1.0 / precision as f64;

    let offset_sand = Point3::new(-w_ / 2.0, 0.0, -h_ / 2.0);
    let offset_sea = offset_sand + Point3::new(0.0, coef - 1.0, 0.0);

    let height_sand =
        logistic(w_ * precision as f64, coef) + perlin(0.0, 0.0, 1.5) + offset_sand.y();
    for i in (0..((width - 1) * precision)).rev() {
        for j in 0..((height - 1) * precision) {
            let i = i as f64;
            let j = j as f64;

            let noise_p1 = perlin(i / w_ * r_ * c, j / h_ * r_ * c, frame);
            let noise_p2 = perlin((i + 1.0) / w_ * r_ * c, j / h_ * r_ * c, frame);
            let noise_p3 = perlin((i + 1.0) / w_ * r_ * c, (j + 1.0) / h_ * r_ * c, frame);
            let noise_p4 = perlin(i / w_ * r_ * c, (j + 1.0) / h_ * r_ * c, frame);

            let height_p1 = logistic(i * r_, coef) + perlin(i / w_ * r_, j / h_ * r_, 1.5);
            let height_p2 =
                logistic((i + 1.0) * r_, coef) + perlin((i + 1.0) / w_ * r_, j / h_ * r_, 1.5);
            let height_p3 = logistic((i + 1.0) * r_, coef)
                + perlin((i + 1.0) / w_ * r_, (j + 1.0) / h_ * r_, 1.5);
            let height_p4 = logistic(i * r_, coef) + perlin(i / w_ * r_, (j + 1.0) / h_ * r_, 1.5);

            //sand
            let p1_s = Point3::new(i * r_, height_p1, j * r_) + offset_sand;
            let p2_s = Point3::new((i + 1.0) * r_, height_p2, j * r_) + offset_sand;
            let p3_s = Point3::new((i + 1.0) * r_, height_p3, (j + 1.0) * r_) + offset_sand;
            let p4_s = Point3::new(i * r_, height_p4, (j + 1.0) * r_) + offset_sand;

            let uni_tex = UniformTexture::new(yellow, 1.0, 0.1, 10000.0);
            let tr1 = Triangle::new(Box::new(uni_tex), p1_s, p2_s, p3_s);

            let uni_tex = UniformTexture::new(yellow, 1.0, 0.1, 10000.0);
            let tr2 = Triangle::new(Box::new(uni_tex), p3_s, p4_s, p1_s);

            objects.push(Box::new(tr1));
            objects.push(Box::new(tr2));

            //water
            let p1_w = Point3::new(i * r_, noise_p1, j * r_) + offset_sea;
            let p2_w = Point3::new((i + 1.0) * r_, noise_p2, j * r_) + offset_sea;
            let p3_w = Point3::new((i + 1.0) * r_, noise_p3, (j + 1.0) * r_) + offset_sea;
            let p4_w = Point3::new(i * r_, noise_p4, (j + 1.0) * r_) + offset_sea;

            let uni_tex = WaterTexture::new();
            let tr1 = Triangle::new(Box::new(uni_tex), p1_w, p2_w, p3_w);

            let uni_tex = WaterTexture::new();
            let tr2 = Triangle::new(Box::new(uni_tex), p3_w, p4_w, p1_w);

            objects.push(Box::new(tr1));
            objects.push(Box::new(tr2));

            if j == 0.0 {
                if p1_w.y() >= p1_w.y() && p2_s.y() >= p3_s.y() {
                    let uni_tex = UnderWaterTexture::new();
                    let tr1 = Triangle::new(Box::new(uni_tex), p1_s, p2_s, p2_w);
                    let uni_tex = UnderWaterTexture::new();
                    let tr2 = Triangle::new(Box::new(uni_tex), p2_w, p1_w, p1_s);

                    objects.push(Box::new(tr1));
                    objects.push(Box::new(tr2));
                } else {
                    let uni_tex = UniformTexture::new(yellow, 1.0, 0.0, 10000.0);
                    let tr1 = Triangle::new(
                        Box::new(uni_tex),
                        p1_s,
                        p2_s,
                        Point3::new(p2_s.x(), height_sand, p2_s.z()),
                    );
                    let uni_tex = UniformTexture::new(yellow, 1.0, 0.0, 10000.0);
                    let tr2 = Triangle::new(
                        Box::new(uni_tex),
                        Point3::new(p2_s.x(), height_sand, p2_s.z()),
                        Point3::new(p1_s.x(), height_sand, p1_s.z()),
                        p1_s,
                    );

                    objects.push(Box::new(tr1));
                    objects.push(Box::new(tr2));
                }
            }
        }
    }
    objects
}

pub fn init_obj(obj: &str) -> Vec<Box<dyn Object>> {
    obj_parser(obj)
}
