use crate::image::Image;
use crate::object::Object;
use crate::*;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

pub fn parse_beach(args: &[String]) -> Vec<Box<dyn Object>> {
    let x = args[0].parse::<usize>().unwrap();
    let y = args[1].parse::<usize>().unwrap();
    let frame = args[2].parse::<f64>().unwrap();
    let precision = args[3].parse::<usize>().unwrap();
    let roughness = args[4].parse::<f64>().unwrap();
    let coef = args[5].parse::<f64>().unwrap();
    init_beach(x, y, precision, frame, roughness, coef)
}

pub fn parse_camera(args: &[String]) -> Camera {
    let x_p = args[0].parse::<f64>().unwrap();
    let y_p = args[1].parse::<f64>().unwrap();
    let z_p = args[2].parse::<f64>().unwrap();
    let x_d = args[3].parse::<f64>().unwrap();
    let y_d = args[4].parse::<f64>().unwrap();
    let z_d = args[5].parse::<f64>().unwrap();

    Camera::new(
        Point3::new(x_p, y_p, z_p),
        Point3::new(x_d, y_d, z_d),
        Vector3::new(0.0, 1.0, 0.0),
        1.0,
        0.7,
        0.7,
    )
}
pub fn parse_input(
    args: &[String],
) -> (
    Image,
    Vec<Box<dyn Object>>,
    Vec<Box<dyn Light>>,
    Vec<Camera>,
) {
    let mut objects = Vec::new();
    let mut camera = Vec::new();
    let mut lights: Vec<Box<dyn Light>> = Vec::new();
    let h: i32 = args[0].parse().unwrap();
    let w: i32 = args[1].parse().unwrap();
    let frame: i32 = args[2].parse().unwrap();
    let img = image::Image::new(h, w, 7);
    if args.len() == 3 {
        println!("nothing to render");
    }

    let mut pos = 3;
    pos = cmd_append_beach(pos, args, &mut objects);
    pos = cmd_append_obj(pos, args, &mut objects);
    
    cmd_append_camera(pos, args, &mut camera);

    let pointlight = PointLight::new(Point3::new(0.0, 10000.0, 30.0), 1.0);
    lights.push(Box::new(pointlight));

    (img, objects, lights, camera)
}
//append from comand line
fn cmd_append_beach(pos : usize, args : &[String], objects : &mut Vec<Box<dyn Object>>) -> usize
{
    let mut pos = pos;
    while pos < args.len() {
        if args[pos].parse::<usize>().is_ok() {
            objects.append(&mut parse_beach(&args[pos..(pos + 6)]));
            pos += 6;
        } else {
            break;
        }
    }
    pos
}
fn cmd_append_obj(pos : usize, args : &[String], objects : &mut Vec<Box<dyn Object>>) -> usize
{
    let mut pos = pos;
    while pos < args.len() {
        if args[pos].parse::<f64>().is_ok() {
            break;
        }
        objects.append(&mut init_obj(&args[pos]));
        pos += 1;
    }
    pos
}

fn cmd_append_camera(pos : usize, args : &[String], camera : &mut Vec<Camera>) -> usize
{
    let mut pos = pos;
    camera.push(parse_camera(&args[pos..(pos + 6)]));
    pos += 6;
    while pos < args.len() {
        match &args[pos][0..4] {
            "--3d" => camera.push(camera[0].clone_x(1.0)),
            &_ => {}
        }
    }
    pos
}

pub fn parse_file(
    file_name: &str,
) -> (
    Image,
    Vec<Box<dyn Object>>,
    Vec<Box<dyn Light>>,
    Vec<Camera>,
) {
    let mut objects = Vec::new();
    let mut camera = Vec::new();
    let mut lights: Vec<Box<dyn Light>> = Vec::new();
    let mut h = 0;
    let mut w = 0;

    let file = File::open(file_name).unwrap();
    let reader = BufReader::new(file);
    for (_, line) in reader.lines().enumerate() {
        let line = line.unwrap();
        match &line[0..3] {
            "C: " => file_append_camera(line, &mut camera),
            "O: " => file_append_obj(line, &mut objects),
            "S: " => file_append_beach(line, &mut objects),
            "H: " => { h = file_get_size(line); }
            "W: " => { w = file_get_size(line); }
            _ => { println!("nothing to do") }
        }
    }
    let img = image::Image::new(h, w, 7);

    let pointlight = PointLight::new(Point3::new(25.0, 100.0, -25.0), 1.0);
    lights.push(Box::new(pointlight));
    (img, objects, lights, camera)
}

//appand from file
fn file_get_size(line : String) -> i32
{
    line.split(' ')
        .map(|x| x.to_string())
        .collect::<Vec<String>>()[1]
        .parse()
        .unwrap()
}

fn file_append_beach(line : String, objects : &mut Vec<Box<dyn Object>>)
{
    objects.append(&mut parse_beach(
            &line
            .split(' ')
            .map(|x| x.to_string())
            .collect::<Vec<String>>()[1..7],
    ))
}

fn file_append_obj(line : String, objects : &mut Vec<Box<dyn Object>>)
{
    objects.append(&mut init_obj(
            &line
            .split(' ')
            .map(|x| x.to_string())
            .collect::<Vec<String>>()[1],
    ))
}

fn file_append_camera(line : String, camera : &mut Vec<Camera>)
{
    camera.push(parse_camera(
            &line
            .split(' ')
            .map(|x| x.to_string())
            .collect::<Vec<String>>()[1..],
    ));
}


pub fn input_parser_dispatcher(
    args: &[String],
) -> Option<(
    Image,
    Vec<Box<dyn Object>>,
    Vec<Box<dyn Light>>,
    Vec<Camera>,
)> {
    match args.len() {
        0 => {
            println!(
                "example sea: 300 400 20 20 1 3 7.0 3.0 0.0 0.0 0.0 0.0 0.0 1.0 100
example object: 300 400 boatpatrick -10.0 10.0 30.0 0.0 0.0 15.0"
            );
            None
        }
        1 => Some(parse_file(&args[0])),
        _ => Some(parse_input(args)),
    }
}
